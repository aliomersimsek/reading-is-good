FROM openjdk:11.0.12-jre
EXPOSE 8080
ARG JAR_FILE=target/case.aliomers-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} case.aliomers.jar
ENTRYPOINT ["java","-jar","case.aliomers.jar"]
