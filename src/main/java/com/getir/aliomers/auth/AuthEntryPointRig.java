package com.getir.aliomers.auth;

import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthEntryPointRig implements AuthenticationEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthEntryPointRig.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        LOGGER.error("Unauthorized error: ", authException.getMessage());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        response.getWriter().write(new CommonExceptionResponse(ErrorCodesContainer.USER_ERROR.E01000, "Unauthorized").toJsonString());
        response.getWriter().flush();
        response.getWriter().close();
    }

}
