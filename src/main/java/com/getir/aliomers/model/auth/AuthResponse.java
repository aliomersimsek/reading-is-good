package com.getir.aliomers.model.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResponse {

    @ApiModelProperty(value = "User Id")
    private String id;

    @ApiModelProperty(value = "User Email address")
    private String email;

    @ApiModelProperty(value = "User First Name")
    private String firstname;

    @ApiModelProperty(value = "User Last Name")
    private String lastname;

    @ApiModelProperty(value = "JWT Token")
    private String jwt;

}
