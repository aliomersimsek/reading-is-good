package com.getir.aliomers.model.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.getir.aliomers.constant.RegexConstant.EMAIL_REGEX;

@Getter
@Setter
public class AuthRequest {

    @NotNull
    @NotEmpty
    @Pattern(message = "Please enter a valid email address", regexp = EMAIL_REGEX)
    @Size(min = 2, max = 64)
    @ApiModelProperty(value = "Email address", required = true)
    private String email;

    @NotEmpty
    @Size(min = 8)
    @ApiModelProperty(value = "password", required = true)
    private String password;

}
