package com.getir.aliomers.model.book;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class BookForm {

    @NotNull
    @Size(min = 2, max = 100)
    @ApiModelProperty(value = "Book Name", required = true)
    private String name;

    @NotNull
    @Size(min = 2, max = 50)
    @ApiModelProperty(value = "Book ISBN", required = true)
    private String isbn;

    @NotNull
    @Size(min = 2, max = 50)
    @ApiModelProperty(value = "Author", required = true)
    private String author;

    @NotNull
    @Size(min = 2, max = 50)
    @ApiModelProperty(value = "Publisher", required = true)
    private String publisher;

    @NotNull
    @ApiModelProperty(value = "Number of Stock", required = true)
    private Long numberOfStock;

    @NotNull
    @ApiModelProperty(value = "Book Price", required = true)
    private BigDecimal price;

}
