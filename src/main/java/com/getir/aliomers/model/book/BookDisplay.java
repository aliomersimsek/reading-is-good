package com.getir.aliomers.model.book;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BookDisplay extends BookForm {

    @ApiModelProperty(value = "Book Id")
    private String uid;

    @ApiModelProperty(value = "Book Creation Date")
    private Date creationDate;

    @ApiModelProperty(value = "Book Update Date")
    private Date updateDate;

}
