package com.getir.aliomers.model.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class StatisticsDisplay {

    @ApiModelProperty(value = "Month")
    private String month;

    @ApiModelProperty(value = "Total Order Count")
    private long totalOrderCount;

    @ApiModelProperty(value = "Total Book Count")
    private long totalBookCount;

    @ApiModelProperty(value = "Total Purchased Amount")
    private BigDecimal totalPurchasedAmount;

}
