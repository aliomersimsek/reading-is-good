package com.getir.aliomers.model.order;

import com.getir.aliomers.model.Month;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class OrderDisplay extends OrderForm {

    @ApiModelProperty(value = "Order Id")
    private String uid;

    @ApiModelProperty(value = "Order Number")
    private String number;

    @ApiModelProperty(value = "Order Month")
    private Month orderMonth;

    @ApiModelProperty(value = "Total Amount")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "Book Creation Date")
    private Date creationDate;

    @ApiModelProperty(value = "Book Update Date")
    private Date updateDate;

}
