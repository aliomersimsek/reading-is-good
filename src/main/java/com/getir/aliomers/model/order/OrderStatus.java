package com.getir.aliomers.model.order;

import java.util.ArrayList;
import java.util.List;

public enum OrderStatus {

    CREATED,
    SHIPPING,
    COMPLETED,
    CANCELED,
    ;

    public static OrderStatus get(String status) {
        for (OrderStatus orderStatus : OrderStatus.values())
            if (String.valueOf(orderStatus).equals(status))
                return orderStatus;
        return null;
    }

    public static List<String> strValues() {
        List<String> res = new ArrayList<>();
        for (OrderStatus orderStatus : OrderStatus.values())
            res.add(orderStatus.name());
        return res;
    }
}
