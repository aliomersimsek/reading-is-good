package com.getir.aliomers.model.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class OrderForm {

    @NotNull
    @ApiModelProperty(value = "Book ID", required = true)
    private String bookId;

    @NotNull
    @ApiModelProperty(value = "Customer ID", required = true)
    private String customerId;

    @Min(1)
    @ApiModelProperty(value = "Count of Book", required = true)
    private Long bookCount;

    @ApiModelProperty(value = "Order Status")
    private OrderStatus status = OrderStatus.CREATED;

    @NotNull
    @ApiModelProperty(value = "Order Date", required = true)
    private Date orderDate;

}
