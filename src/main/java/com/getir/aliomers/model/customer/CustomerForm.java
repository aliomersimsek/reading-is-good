package com.getir.aliomers.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.getir.aliomers.constant.RegexConstant.EMAIL_REGEX;

@Getter
@Setter
public class CustomerForm {

    @NotNull
    @Pattern(message = "Please enter a valid email address", regexp = EMAIL_REGEX)
    @Size(min = 2, max = 100)
    @ApiModelProperty(value = "Customer Email", required = true)
    private String email;

    @NotNull
    @Size(min = 2, max = 50)
    @ApiModelProperty(value = "Customer First Name", required = true)
    private String firstname;

    @NotNull
    @Size(min = 2, max = 50)
    @ApiModelProperty(value = "Customer Last Name", required = true)
    private String lastname;

}
