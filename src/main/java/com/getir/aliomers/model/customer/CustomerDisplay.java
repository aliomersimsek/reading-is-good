package com.getir.aliomers.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class CustomerDisplay extends CustomerForm {

    @ApiModelProperty(value = "Customer Id")
    private String uid;

    @ApiModelProperty(value = "Customer Creation Date")
    private Date creationDate;

    @ApiModelProperty(value = "Customer Update Date")
    private Date updateDate;

}
