package com.getir.aliomers.model.pagination;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class PaginationResponse {

    private List list;
    private int currentPage;
    private long totalItems;
    private int totalPages;

}
