package com.getir.aliomers.model.mapper;

import com.getir.aliomers.auth.AuthUserDetailsRig;
import com.getir.aliomers.model.auth.AuthResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface AuthenticationMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "firstName", target = "firstname"),
            @Mapping(source = "lastName", target = "lastname")
    })
    AuthResponse toAuthResponse(AuthUserDetailsRig s);
}
