package com.getir.aliomers.model.mapper;

import com.getir.aliomers.entity.Book;
import com.getir.aliomers.model.book.BookDisplay;
import com.getir.aliomers.model.book.BookForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface BookMapper {

    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "isbn", target = "isbn"),
            @Mapping(source = "author", target = "author"),
            @Mapping(source = "publisher", target = "publisher"),
            @Mapping(source = "numberOfStock", target = "numberOfStock"),
            @Mapping(source = "price", target = "price")
    })
    Book toBook(BookForm s);

    default List<BookDisplay> toBookDisplayList(List<Book> bookList) {
        return bookList.stream().map(this::toBookDisplay).collect(Collectors.toList());
    }

    @Mappings({
            @Mapping(source = "uid", target = "uid"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "isbn", target = "isbn"),
            @Mapping(source = "author", target = "author"),
            @Mapping(source = "publisher", target = "publisher"),
            @Mapping(source = "numberOfStock", target = "numberOfStock"),
            @Mapping(source = "price", target = "price"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "updateDate", target = "updateDate")
    })
    BookDisplay toBookDisplay(Book s);

}
