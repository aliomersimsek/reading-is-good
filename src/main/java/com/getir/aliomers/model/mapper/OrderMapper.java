package com.getir.aliomers.model.mapper;

import com.getir.aliomers.entity.Order;
import com.getir.aliomers.model.order.OrderDisplay;
import com.getir.aliomers.model.order.OrderForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mappings({
            @Mapping(source = "bookId", target = "bookId"),
            @Mapping(source = "customerId", target = "customerId"),
            @Mapping(source = "bookCount", target = "bookCount"),
            @Mapping(source = "status", target = "status"),
            @Mapping(source = "orderDate", target = "orderDate")
    })
    Order toOrder(OrderForm s);

    default List<OrderDisplay> toOrderDisplayList(List<Order> orderList) {
        return orderList.stream().map(this::toOrderDisplay).collect(Collectors.toList());
    }

    @Mappings({
            @Mapping(source = "uid", target = "uid"),
            @Mapping(source = "number", target = "number"),
            @Mapping(source = "bookId", target = "bookId"),
            @Mapping(source = "customerId", target = "customerId"),
            @Mapping(source = "bookCount", target = "bookCount"),
            @Mapping(source = "totalAmount", target = "totalAmount"),
            @Mapping(source = "status", target = "status"),
            @Mapping(source = "orderDate", target = "orderDate"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "updateDate", target = "updateDate")
    })
    OrderDisplay toOrderDisplay(Order s);

}
