package com.getir.aliomers.model.mapper;

import com.getir.aliomers.entity.Customer;
import com.getir.aliomers.model.customer.CustomerDisplay;
import com.getir.aliomers.model.customer.CustomerForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    @Mappings({
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "firstname", target = "firstname"),
            @Mapping(source = "lastname", target = "lastname")
    })
    Customer toCustomer(CustomerForm s);

    default List<CustomerDisplay> toCustomerDisplayList(List<Customer> customerList) {
        return customerList.stream().map(this::toCustomerDisplay).collect(Collectors.toList());
    }

    @Mappings({
            @Mapping(source = "uid", target = "uid"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "firstname", target = "firstname"),
            @Mapping(source = "lastname", target = "lastname"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "updateDate", target = "updateDate")
    })
    CustomerDisplay toCustomerDisplay(Customer s);

}
