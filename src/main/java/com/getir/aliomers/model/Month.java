package com.getir.aliomers.model;

public enum Month {

    JANUARY(0),
    FEBRUARY(1),
    MARCH(2),
    APRIL(3),
    MAY(4),
    JUNE(5),
    JULY(6),
    AUGUST(7),
    SEPTEMBER(8),
    OCTOBER(9),
    NOVEMBER(10),
    DECEMBER(11);

    private int number;

    Month(int number) {
        this.number = number;
    }

    public int getMonthNumber() {
        return number;
    }

    public static Month getByNumber(int number) {
        for (Month month : Month.values())
            if (month.number == number)
                return month;
        return null;
    }

    public static Month get(String str) {
        for (Month month : Month.values())
            if (month.name().equals(str))
                return month;
        return null;
    }
}
