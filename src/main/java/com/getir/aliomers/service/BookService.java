package com.getir.aliomers.service;

import com.getir.aliomers.entity.Book;
import com.getir.aliomers.model.book.BookForm;
import com.getir.aliomers.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Optional<Book> findByName(String name) {
        return bookRepository.findByName(name);
    }

    public Optional<Book> findByIsbn(String isbn) {
        return bookRepository.findByIsbn(isbn);
    }

    public Book create(Book book) {
        book.setUid(UUID.randomUUID().toString());
        book.setCreationDate(new Date());
        book.setUpdateDate(new Date());
        return bookRepository.save(book);
    }

    public Optional<Book> findByUid(String id) {
        return bookRepository.findByUid(id);
    }

    public Page<Book> list(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    public Book update(Book bookInDb, BookForm bookForm) {
        Book _book = bookInDb;
        if (bookForm.getName() != null && !bookForm.getName().trim().isEmpty())
            _book.setName(bookForm.getName());
        if (bookForm.getIsbn() != null && !bookForm.getIsbn().trim().isEmpty())
            _book.setIsbn(bookForm.getIsbn());
        if (bookForm.getAuthor() != null && !bookForm.getAuthor().trim().isEmpty())
            _book.setAuthor(bookForm.getAuthor());
        if (bookForm.getPublisher() != null && !bookForm.getPublisher().trim().isEmpty())
            _book.setPublisher(bookForm.getPublisher());
        if (bookForm.getNumberOfStock() != null && bookForm.getNumberOfStock() > 0)
            _book.setNumberOfStock(bookForm.getNumberOfStock());
        if (bookForm.getPrice() != null && bookForm.getPrice().compareTo(new BigDecimal("0.0")) > 0)
            _book.setPrice(bookForm.getPrice());
        _book.setUpdateDate(new Date());
        return bookRepository.save(_book);
    }

    public Book updateStock(Book bookInDb, Long numberOfStock) {
        Book _book = bookInDb;
        _book.setNumberOfStock(numberOfStock);
        _book.setUpdateDate(new Date());
        return bookRepository.save(_book);
    }

    public void deleteById(String uid) {
        bookRepository.deleteByUid(uid);
    }

}
