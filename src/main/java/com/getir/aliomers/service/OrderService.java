package com.getir.aliomers.service;

import com.getir.aliomers.entity.Book;
import com.getir.aliomers.entity.Order;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.book.BookForm;
import com.getir.aliomers.model.order.OrderStatus;
import com.getir.aliomers.repository.OrderRepository;
import com.getir.aliomers.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderService {

    final static Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private BookService bookService;


    @Transactional
    public synchronized Optional<Object> create(Order order) {

        Optional<Book> bookOpt = bookService.findByUid(order.getBookId());
        if (bookOpt.isEmpty()) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04001.getMessage(order.getBookId()));
            return Optional.of(new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.NOT_FOUND,
                            ErrorCodesContainer.ORDER_ERROR.E04001,
                            order.getBookId()),
                    HttpStatus.NOT_FOUND
            ));
        }

        Book bookInDb = bookOpt.get();
        if (bookInDb.getNumberOfStock() == 0) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04005.getMessage(bookInDb.getName()));
            return Optional.of(new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.BAD_REQUEST,
                            ErrorCodesContainer.ORDER_ERROR.E04005,
                            bookInDb.getName()),
                    HttpStatus.BAD_REQUEST
            ));
        }

        if (bookInDb.getNumberOfStock() < order.getBookCount()) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04006.getMessage(bookInDb.getNumberOfStock().toString()));
            return Optional.of(new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.BAD_REQUEST,
                            ErrorCodesContainer.ORDER_ERROR.E04006,
                            bookInDb.getNumberOfStock().toString()),
                    HttpStatus.BAD_REQUEST
            ));
        }

        decreaseStock(bookInDb, order.getBookCount());

        order.setTotalAmount(bookInDb.getPrice().multiply(BigDecimal.valueOf(order.getBookCount())));
        Order inserted = insert(order);
        return Optional.of(inserted);
    }

    private synchronized void decreaseStock(Book bookInDb, Long count) {
        BookForm bookForm = new BookForm();
        bookForm.setNumberOfStock(bookInDb.getNumberOfStock() - count);
        bookService.update(bookInDb, bookForm);
    }

    public Order insert(Order order) {
        order.setUid(UUID.randomUUID().toString());
        order.setNumber(UUID.randomUUID().toString());
        order.setOrderMonth(DateUtil.getMonth(order.getOrderDate()));
        order.setCreationDate(new Date());
        order.setUpdateDate(new Date());
        return orderRepository.save(order);
    }

    @Transactional
    public synchronized Optional<Object> cancel(Order order) {

        if (!OrderStatus.CREATED.equals(order.getStatus())) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04007.getMessage(order.getStatus().toString()));
            return Optional.of(new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.BAD_REQUEST,
                            ErrorCodesContainer.ORDER_ERROR.E04007,
                            order.getStatus().toString()),
                    HttpStatus.BAD_REQUEST
            ));
        }

        Optional<Book> bookOpt = bookService.findByUid(order.getBookId());
        if (bookOpt.isEmpty()) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04001.getMessage(order.getBookId()));
            return Optional.of(new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.NOT_FOUND,
                            ErrorCodesContainer.ORDER_ERROR.E04001,
                            order.getBookId()),
                    HttpStatus.NOT_FOUND
            ));
        }

        Book bookInDb = bookOpt.get();

        increaseStock(bookInDb, order.getBookCount());

        Order updated = updateOrderStatus(order, OrderStatus.CANCELED);
        return Optional.of(updated);
    }

    private synchronized void increaseStock(Book bookInDb, Long count) {
        BookForm bookForm = new BookForm();
        bookForm.setNumberOfStock(bookInDb.getNumberOfStock() + count);
        bookService.update(bookInDb, bookForm);
    }

    public Order updateOrderStatus(Order orderInDb, OrderStatus status) {
        Order _order = orderInDb;
        _order.setStatus(status);
        _order.setUpdateDate(new Date());
        return orderRepository.save(_order);
    }

    public Optional<Order> findByUid(String id) {
        return orderRepository.findByUid(id);
    }

    public Page<Order> list(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }

    public Page<Order> listByCustomer(String customerId, Pageable pageable) {
        return orderRepository.findAllByCustomerId(customerId, pageable);
    }

    public Page<Order> listByDateInterval(Date from, Date to, Pageable pageable) {
        return orderRepository.findAllByOrderDateBetween(from, to, pageable);
    }

    public List<Order> getAllOrdersByBook(String bookId) {
        return orderRepository.findAllByBookId(bookId);
    }

    public List<Order> getAllOrdersByCustomer(String customerId) {
        return orderRepository.findAllByCustomerId(customerId);
    }
}
