package com.getir.aliomers.service;

import com.getir.aliomers.entity.Customer;
import com.getir.aliomers.model.customer.CustomerForm;
import com.getir.aliomers.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Optional<Customer> findByEmail(String email) {
        return customerRepository.findByEmail(email);
    }

    public Customer create(Customer customer) {
        customer.setUid(UUID.randomUUID().toString());
        customer.setCreationDate(new Date());
        customer.setUpdateDate(new Date());
        return customerRepository.save(customer);
    }

    public Optional<Customer> findByUid(String uid) {
        return customerRepository.findByUid(uid);
    }

    public Page<Customer> list(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    public Customer update(Customer customerInDb, CustomerForm customerForm) {
        Customer _customer = customerInDb;
        if (customerForm.getEmail() != null && !customerForm.getEmail().trim().isEmpty())
            _customer.setEmail(customerForm.getEmail());
        if (customerForm.getFirstname() != null && !customerForm.getFirstname().trim().isEmpty())
            _customer.setFirstname(customerForm.getFirstname());
        if (customerForm.getLastname() != null && !customerForm.getLastname().trim().isEmpty())
            _customer.setLastname(customerForm.getLastname());
        _customer.setUpdateDate(new Date());
        return customerRepository.save(_customer);
    }

    public void deleteById(String uid) {
        customerRepository.deleteByUid(uid);
    }

}
