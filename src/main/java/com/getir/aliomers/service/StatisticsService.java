package com.getir.aliomers.service;

import com.getir.aliomers.model.statistics.StatisticsDisplay;
import com.getir.aliomers.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticsService {

    final static Logger LOGGER = LoggerFactory.getLogger(StatisticsService.class);

    @Autowired
    private OrderRepository orderRepository;


    public List<StatisticsDisplay> getCustomersMonthlyStats(String customerId) {
        return orderRepository.getStats(customerId);
    }

}
