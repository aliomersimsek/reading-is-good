package com.getir.aliomers.service;

import com.getir.aliomers.auth.AuthUserDetailsRig;
import com.getir.aliomers.entity.User;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public AuthUserDetailsRig loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(ErrorCodesContainer.USER_ERROR.E01000.getMessage(email)));

        return AuthUserDetailsRig.build(user);
    }

}
