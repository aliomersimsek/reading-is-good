package com.getir.aliomers.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@Document(collection = "customer")
public class Customer {

    @Id
    private String _id;

    private String uid;

    @Indexed(unique = true)
    private String email;

    private String firstname;

    private String lastname;

    private Date creationDate;

    private Date updateDate;

}
