package com.getir.aliomers.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.data.mongodb.core.mapping.FieldType.DECIMAL128;

@Getter
@Setter
@Document(collection = "book")
public class Book {

    @Id
    private String _id;

    private String uid;

    @Indexed(unique = true)
    private String name;

    @Indexed(unique = true)
    private String isbn;

    private String author;

    private String publisher;

    private Long numberOfStock;

    @Field(targetType = DECIMAL128)
    private BigDecimal price;

    private Date creationDate;

    private Date updateDate;

}
