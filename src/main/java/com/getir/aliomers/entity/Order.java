package com.getir.aliomers.entity;

import com.getir.aliomers.model.Month;
import com.getir.aliomers.model.order.OrderStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.data.mongodb.core.mapping.FieldType.DECIMAL128;

@Getter
@Setter
@Document(collection = "order")
public class Order {

    @Id
    private String _id;

    private String uid;

    @Indexed(unique = true)
    private String number;

    private String bookId;

    private String customerId;

    private Long bookCount;

    @Field(targetType = DECIMAL128)
    private BigDecimal totalAmount;

    private OrderStatus status;

    private Month orderMonth;

    private Date orderDate;

    private Date creationDate;

    private Date updateDate;

}
