package com.getir.aliomers.exception;

public interface IErrorCodes {

    String getCode();

    String getMessage();

    default String getMessage(String... params) {
        StringBuilder message = new StringBuilder(this.getMessage());
        if (params == null || params.length == 0)
            return message.toString();
        for (int i = 0; i < params.length; i++) {
            String key = "{" + (i + 1) + "}";
            int index = message.indexOf(key);
            //message has not any parametric part or params count is more than parametric parts
            if (index == -1)
                break;

            String replaceWithMe = params[i] != null ? params[i] : "";
            message.replace(index, index + key.length(), replaceWithMe);
        }
        return message.toString();
    }

}
