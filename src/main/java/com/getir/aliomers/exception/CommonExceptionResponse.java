package com.getir.aliomers.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Getter
@Setter
public class CommonExceptionResponse {

    private final long timestamp;

    private final String error;

    private final String message;

    private final IErrorCodes code;

    public CommonExceptionResponse(IErrorCodes code, String... params) {
        this.message = code.getMessage(params);
        this.error = null;
        this.timestamp = new Date().getTime();
        this.code = code;
    }

    public CommonExceptionResponse(HttpStatus httpStatus, IErrorCodes code, String... params) {
        this.message = code.getMessage(params);
        this.error = httpStatus != null ? httpStatus.getReasonPhrase() : null;
        this.timestamp = new Date().getTime();
        this.code = code;
    }

    public String toJsonString() {
        return "{\"message\":\"" + message + "\", \"timestamp\":\"" + timestamp + "\"}";
    }
}
