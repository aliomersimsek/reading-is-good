package com.getir.aliomers.exception;

public class ErrorCodesContainer {

    public enum USER_ERROR implements IErrorCodes {
        E01000("Error : {1}."),
        E01001("Invalid Login!"),
        ;

        private String code;
        private String message;

        USER_ERROR(String message) {
            this.code = String.valueOf(this);
            this.message = message;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

    public enum CUSTOMER_ERROR implements IErrorCodes {
        E02000("Error: {1}"),
        E02001("This email used by another customer: {1}"),
        E02002("Customer is not found with id: {1}"),
        E02003("It is not possible to delete the customer({1}) because there are orders belongs to!"),
        ;

        private String code;
        private String message;

        CUSTOMER_ERROR(String message) {
            this.code = String.valueOf(this);
            this.message = message;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

    public enum BOOK_ERROR implements IErrorCodes {
        E03000("Error: {1}"),
        E03001("This book is already added with name: {1}"),
        E03002("Book is not found with id: {1}"),
        E03003("This book is already added with ISBN: {1}"),
        E03004("It is not possible to delete the book({1}) because there are orders belongs to!");

        private String code;
        private String message;

        BOOK_ERROR(String message) {
            this.code = String.valueOf(this);
            this.message = message;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

    public enum ORDER_ERROR implements IErrorCodes {
        E04000("Error: {1}"),
        E04001("Book is not found with id: {1}"),
        E04002("Customer is not found with id: {1}"),
        E04003("Order is not found with id: {1}"),
        E04005("This book is out of stock that its name is: {1}"),
        E04006("Stock of this book is less than requested. Remaining stock: {1}"),
        E04007("The status of the order to be canceled must be 'CREATED'. This order's status is '{1}'"),
        E04010("To date must be greater than from date! (From: '{1}' - To: '{2}')"),
        E04011("Order status must be valid! (Valid Order Statuses : {1})"),
        E04012("Order current status must be changeable! (Changeable Order Statuses : {1})"),

        ;

        private String code;
        private String message;

        ORDER_ERROR(String message) {
            this.code = String.valueOf(this);
            this.message = message;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

    public enum STATISTICS_ERROR implements IErrorCodes {
        E05000("Error: {1}"),
        E05001("Customer is not found with id: {1}"),

        ;

        private String code;
        private String message;

        STATISTICS_ERROR(String message) {
            this.code = String.valueOf(this);
            this.message = message;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }
}
