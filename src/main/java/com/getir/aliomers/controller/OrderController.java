package com.getir.aliomers.controller;

import com.getir.aliomers.entity.Book;
import com.getir.aliomers.entity.Customer;
import com.getir.aliomers.entity.Order;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.mapper.OrderMapper;
import com.getir.aliomers.model.order.OrderDisplay;
import com.getir.aliomers.model.order.OrderForm;
import com.getir.aliomers.model.order.OrderStatus;
import com.getir.aliomers.model.pagination.PaginationResponse;
import com.getir.aliomers.service.BookService;
import com.getir.aliomers.service.CustomerService;
import com.getir.aliomers.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@Api(produces = "application/json", tags = {"04 - Order Controller"})
@RequestMapping("/order")
public class OrderController {

    final static Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private BookService bookService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderMapper orderMapper;


    @PostMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = OrderDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Create Order", notes = "Creates new order")
    public ResponseEntity createOrder(@Valid @RequestBody OrderForm orderRequest) {
        try {
            Optional<Customer> customerOpt = customerService.findByUid(orderRequest.getCustomerId());
            if (customerOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04002.getMessage(orderRequest.getCustomerId()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.ORDER_ERROR.E04002,
                                orderRequest.getCustomerId()),
                        HttpStatus.NOT_FOUND
                );
            }

            Optional<Book> bookOpt = bookService.findByUid(orderRequest.getBookId());
            if (bookOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04001.getMessage(orderRequest.getBookId()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.ORDER_ERROR.E04001,
                                orderRequest.getBookId()),
                        HttpStatus.NOT_FOUND
                );
            }

            Optional<Object> result = orderService.create(orderMapper.toOrder(orderRequest));
            Object resultObject = result.get();
            if (resultObject instanceof ResponseEntity) {
                return (ResponseEntity) resultObject;
            }

            Order createdOrder = (Order) resultObject;
            return new ResponseEntity<>(
                    orderMapper.toOrderDisplay(createdOrder),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = OrderDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Cancel Order", notes = "Cancels order")
    public ResponseEntity cancelOrder(@Valid @PathVariable String id) {

        try {
            Optional<Order> orderOpt = orderService.findByUid(id);
            if (orderOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04003.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.ORDER_ERROR.E04003,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            Order orderInDb = orderOpt.get();
            Optional<Object> result = orderService.cancel(orderInDb);
            Object resultObject = result.get();
            if (resultObject instanceof ResponseEntity) {
                return (ResponseEntity) resultObject;
            }

            Order canceledOrder = (Order) resultObject;
            return new ResponseEntity<>(
                    orderMapper.toOrderDisplay(canceledOrder),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Orders With Pagination", notes = "Lists all orders with pagination")
    public ResponseEntity listOrders(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {

        try {
            Pageable paging = PageRequest.of(page, size);
            Page<Order> pageData = orderService.list(paging);
            List<Order> content = pageData.getContent();

            return new ResponseEntity<>(PaginationResponse.builder()
                    .list(orderMapper.toOrderDisplayList(content))
                    .currentPage(pageData.getNumber())
                    .totalItems(pageData.getTotalElements())
                    .totalPages(pageData.getTotalPages())
                    .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = OrderDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Get Order By Id", notes = "Gets order by id")
    public ResponseEntity getOrderById(@Valid @PathVariable String id) {
        try {

            Optional<Order> orderInDb = orderService.findByUid(id);
            if (orderInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04003.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.ORDER_ERROR.E04003,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            return new ResponseEntity<>(
                    orderMapper.toOrderDisplay(orderInDb.get()),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/dateInterval", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Orders By Date Interval With Pagination", notes = "Lists all orders by date interval with pagination")
    public ResponseEntity listOrdersByDateInterval(
            @RequestParam Long from,
            @RequestParam Long to,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {

        try {
            if (from >= to) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04010.getMessage(new Date(from).toString(), new Date(to).toString()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.ORDER_ERROR.E04010,
                                new Date(from).toString(), new Date(to).toString()),
                        HttpStatus.BAD_REQUEST
                );
            }

            Pageable paging = PageRequest.of(page, size);
            Page<Order> pageData = orderService.listByDateInterval(new Date(from), new Date(to), paging);
            List<Order> content = pageData.getContent();

            return new ResponseEntity<>(PaginationResponse.builder()
                    .list(orderMapper.toOrderDisplayList(content))
                    .currentPage(pageData.getNumber())
                    .totalItems(pageData.getTotalElements())
                    .totalPages(pageData.getTotalPages())
                    .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @PatchMapping(path = "/{id}/status", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = OrderDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Update Order Status", notes = "Updates order status")
    public ResponseEntity updateOrderStatus(
            @Valid @PathVariable String id,
            @RequestParam String status) {

        try {
            final List<OrderStatus> changeableStatuses = Arrays.asList(OrderStatus.CREATED, OrderStatus.SHIPPING);

            if (status == null || OrderStatus.get(status) == null) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04011.getMessage(OrderStatus.strValues().toString()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.ORDER_ERROR.E04011,
                                OrderStatus.strValues().toString()),
                        HttpStatus.BAD_REQUEST
                );
            }
            OrderStatus orderStatus = OrderStatus.get(status);

            Optional<Order> orderOpt = orderService.findByUid(id);
            if (orderOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04003.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.ORDER_ERROR.E04003,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }
            Order orderInDb = orderOpt.get();

            if (!changeableStatuses.contains(orderInDb.getStatus())) {
                LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04012.getMessage(changeableStatuses.toString()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.ORDER_ERROR.E04012,
                                changeableStatuses.toString()),
                        HttpStatus.BAD_REQUEST
                );
            }

            if (OrderStatus.CANCELED.equals(orderStatus)) {
                Optional<Object> result = orderService.cancel(orderInDb);
                Object resultObject = result.get();
                if (resultObject instanceof ResponseEntity) {
                    return (ResponseEntity) resultObject;
                }

                Order canceledOrder = (Order) resultObject;
                return new ResponseEntity<>(
                        orderMapper.toOrderDisplay(canceledOrder),
                        HttpStatus.OK
                );
            }

            Order updated = orderService.updateOrderStatus(orderInDb, orderStatus);
            return new ResponseEntity<>(
                    orderMapper.toOrderDisplay(updated),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.ORDER_ERROR.E04000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.ORDER_ERROR.E04000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }
}
