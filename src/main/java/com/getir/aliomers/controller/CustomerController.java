package com.getir.aliomers.controller;

import com.getir.aliomers.entity.Customer;
import com.getir.aliomers.entity.Order;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.customer.CustomerDisplay;
import com.getir.aliomers.model.customer.CustomerForm;
import com.getir.aliomers.model.mapper.CustomerMapper;
import com.getir.aliomers.model.mapper.OrderMapper;
import com.getir.aliomers.model.pagination.PaginationResponse;
import com.getir.aliomers.service.CustomerService;
import com.getir.aliomers.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Api(produces = "application/json", tags = {"02 - Customer Controller"})
@RequestMapping("/customer")
public class CustomerController {

    final static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private OrderMapper orderMapper;


    @PostMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Create Customer", notes = "Creates new customer")
    public ResponseEntity createCustomer(@Valid @RequestBody CustomerForm customerRequest) {
        try {
            Optional<Customer> existCustomer = customerService.findByEmail(customerRequest.getEmail());
            if (!existCustomer.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02001.getMessage(customerRequest.getEmail()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02001,
                                customerRequest.getEmail()),
                        HttpStatus.BAD_REQUEST
                );
            }

            Customer savedCustomer = customerService.create(customerMapper.toCustomer(customerRequest));
            return new ResponseEntity<>(
                    customerMapper.toCustomerDisplay(savedCustomer),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Customers With Pagination", notes = "Lists all customers with pagination")
    public ResponseEntity listCustomers(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {

        try {
            Pageable paging = PageRequest.of(page, size);
            Page<Customer> pageData = customerService.list(paging);
            List<Customer> content = pageData.getContent();

            return new ResponseEntity<>(PaginationResponse.builder()
                    .list(customerMapper.toCustomerDisplayList(content))
                    .currentPage(pageData.getNumber())
                    .totalItems(pageData.getTotalElements())
                    .totalPages(pageData.getTotalPages())
                    .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Get Customer By Id", notes = "Gets customer by id")
    public ResponseEntity getCustomerById(@Valid @PathVariable String id) {
        try {

            Optional<Customer> customerInDb = customerService.findByUid(id);
            if (customerInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            return new ResponseEntity<>(
                    customerMapper.toCustomerDisplay(customerInDb.get()),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @PatchMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Update Customer By Id", notes = "Updates customer by id")
    public ResponseEntity updateCustomerById(
            @RequestBody CustomerForm customerForm,
            @Valid @PathVariable String id) {

        try {
            Optional<Customer> customerInDb = customerService.findByUid(id);
            if (customerInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            Customer updated = customerService.update(customerInDb.get(), customerForm);
            return new ResponseEntity<>(
                    customerMapper.toCustomerDisplay(updated),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Delete Customer By Id", notes = "Deletes customer by id")
    public ResponseEntity deleteCustomerById(@Valid @PathVariable String id) {

        try {

            // There is no customer for the given id {1}
            Optional<Customer> customerInDb = customerService.findByUid(id);
            if (customerInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            List<Order> allOrdersByCustomer = orderService.getAllOrdersByCustomer(customerInDb.get().getUid());
            if (!allOrdersByCustomer.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02003.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02003,
                                id),
                        HttpStatus.BAD_REQUEST
                );
            }

            customerService.deleteById(id);
            return new ResponseEntity<>(
                    "Customer successfuly deleted",
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/{id}/orders", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Customer's Orders", notes = "Get Orders Customer By Id")
    public ResponseEntity getOrdersCustomerById(
            @Valid @PathVariable String id,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {
        try {

            Optional<Customer> customerOpt = customerService.findByUid(id);
            if (customerOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.CUSTOMER_ERROR.E02002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            Customer customerInDb = customerOpt.get();

            Pageable paging = PageRequest.of(page, size);
            Page<Order> pageData = orderService.listByCustomer(customerInDb.getUid(), paging);
            List<Order> content = pageData.getContent();

            return new ResponseEntity<>(PaginationResponse.builder()
                    .list(orderMapper.toOrderDisplayList(content))
                    .currentPage(pageData.getNumber())
                    .totalItems(pageData.getTotalElements())
                    .totalPages(pageData.getTotalPages())
                    .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.CUSTOMER_ERROR.E02000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.CUSTOMER_ERROR.E02000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
