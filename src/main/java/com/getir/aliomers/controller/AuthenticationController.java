package com.getir.aliomers.controller;

import com.getir.aliomers.auth.AuthUserDetailsRig;
import com.getir.aliomers.entity.User;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.auth.AuthRequest;
import com.getir.aliomers.model.auth.AuthResponse;
import com.getir.aliomers.model.mapper.AuthenticationMapper;
import com.getir.aliomers.service.UserService;
import com.getir.aliomers.util.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@Api(produces = "application/json", tags = {"01 - Authentication Controller"})
@RequestMapping("/")
public class AuthenticationController {

    final static Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthenticationMapper authenticationMapper;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping(path = "/login", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AuthResponse.class),
            @ApiResponse(code = 400, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Login", notes = "Authenticates user and returns user's information with JWT Token")
    public ResponseEntity login(@RequestBody AuthRequest authRequest) {

        try {
            Optional<User> userOpt = userService.findByEmail(authRequest.getEmail());
            if (userOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.USER_ERROR.E01001.getMessage());
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.UNAUTHORIZED,
                                ErrorCodesContainer.USER_ERROR.E01001),
                        HttpStatus.UNAUTHORIZED
                );
            }

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authRequest.getEmail(),
                            authRequest.getPassword()
                    )
            );

            AuthUserDetailsRig authUserDetailsRig = (AuthUserDetailsRig) authentication.getPrincipal();

            String jwt = jwtUtil.generateJwtToken(authentication);

            AuthResponse response = authenticationMapper.toAuthResponse(authUserDetailsRig);
            response.setJwt(jwt);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (AuthenticationException e) {
            LOGGER.error(ErrorCodesContainer.USER_ERROR.E01001.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.BAD_REQUEST,
                            ErrorCodesContainer.USER_ERROR.E01001),
                    HttpStatus.BAD_REQUEST
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.USER_ERROR.E01000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.USER_ERROR.E01000),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
