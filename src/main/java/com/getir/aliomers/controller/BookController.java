package com.getir.aliomers.controller;

import com.getir.aliomers.entity.Book;
import com.getir.aliomers.entity.Order;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.book.BookDisplay;
import com.getir.aliomers.model.book.BookForm;
import com.getir.aliomers.model.mapper.BookMapper;
import com.getir.aliomers.model.pagination.PaginationResponse;
import com.getir.aliomers.service.BookService;
import com.getir.aliomers.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@RestController
@Api(produces = "application/json", tags = {"03 - Book Controller"})
@RequestMapping("/book")
public class BookController {

    final static Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private BookMapper bookMapper;


    @PostMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BookDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Create Book", notes = "Creates new book")
    public ResponseEntity createBook(@Valid @RequestBody BookForm bookRequest) {
        try {
            Optional<Book> existBookWithName = bookService.findByName(bookRequest.getName());
            if (!existBookWithName.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03001.getMessage(bookRequest.getName()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.BOOK_ERROR.E03001,
                                bookRequest.getName()),
                        HttpStatus.BAD_REQUEST
                );
            }

            Optional<Book> existBookWithIsbn = bookService.findByIsbn(bookRequest.getIsbn());
            if (!existBookWithIsbn.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03003.getMessage(bookRequest.getIsbn()));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.BOOK_ERROR.E03003,
                                bookRequest.getIsbn()),
                        HttpStatus.BAD_REQUEST
                );
            }

            Book savedBook = bookService.create(bookMapper.toBook(bookRequest));
            return new ResponseEntity<>(
                    bookMapper.toBookDisplay(savedBook),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Books With Pagination", notes = "Lists all books with pagination")
    public ResponseEntity listBooksWithPagination(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {

        try {
            Pageable paging = PageRequest.of(page, size);
            Page<Book> pageData = bookService.list(paging);
            List<Book> content = pageData.getContent();

            return new ResponseEntity<>(PaginationResponse.builder()
                    .list(bookMapper.toBookDisplayList(content))
                    .currentPage(pageData.getNumber())
                    .totalItems(pageData.getTotalElements())
                    .totalPages(pageData.getTotalPages())
                    .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BookDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Get Book By Id", notes = "Gets book by id")
    public ResponseEntity getBookById(@Valid @PathVariable String id) {
        try {

            Optional<Book> bookInDb = bookService.findByUid(id);
            if (bookInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.BOOK_ERROR.E03002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            return new ResponseEntity<>(
                    bookMapper.toBookDisplay(bookInDb.get()),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @PatchMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BookDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Update Book By Id", notes = "Updates book by id")
    public ResponseEntity updateBookById(
            @RequestBody BookForm bookForm,
            @Valid @PathVariable String id) {

        try {
            Optional<Book> bookInDb = bookService.findByUid(id);
            if (bookInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.BOOK_ERROR.E03002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            Book updated = bookService.update(bookInDb.get(), bookForm);
            return new ResponseEntity<>(
                    bookMapper.toBookDisplay(updated),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Delete Book By Id", notes = "Deletes book by id")
    public ResponseEntity deleteBookById(@Valid @PathVariable String id) {

        try {

            // There is no book for the given id {1}
            Optional<Book> bookInDb = bookService.findByUid(id);
            if (bookInDb.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.BOOK_ERROR.E03002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            List<Order> allOrdersByBook = orderService.getAllOrdersByBook(bookInDb.get().getUid());
            if (!allOrdersByBook.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03004.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.BAD_REQUEST,
                                ErrorCodesContainer.BOOK_ERROR.E03004,
                                id),
                        HttpStatus.BAD_REQUEST
                );
            }

            bookService.deleteById(id);
            return new ResponseEntity<>(
                    "Book successfuly deleted",
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @PatchMapping(path = "/{id}/stock", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BookDisplay.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "Update Book Stock", notes = "Updates book stock by id")
    public ResponseEntity updateStockById(
            @NotNull @Min(1) @RequestParam Long numberOfStock,
            @Valid @PathVariable String id) {

        try {
            Optional<Book> bookOpt = bookService.findByUid(id);
            if (bookOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03002.getMessage(id));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.BOOK_ERROR.E03002,
                                id),
                        HttpStatus.NOT_FOUND
                );
            }

            Book bookInDb = bookOpt.get();
            Book updated = bookService.updateStock(bookInDb, numberOfStock);
            return new ResponseEntity<>(
                    bookMapper.toBookDisplay(updated),
                    HttpStatus.OK
            );

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.BOOK_ERROR.E03000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.BOOK_ERROR.E03000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
