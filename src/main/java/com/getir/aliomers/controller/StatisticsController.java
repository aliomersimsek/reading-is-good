package com.getir.aliomers.controller;

import com.getir.aliomers.entity.Customer;
import com.getir.aliomers.exception.CommonExceptionResponse;
import com.getir.aliomers.exception.ErrorCodesContainer;
import com.getir.aliomers.model.mapper.OrderMapper;
import com.getir.aliomers.model.pagination.PaginationResponse;
import com.getir.aliomers.model.statistics.StatisticsDisplay;
import com.getir.aliomers.service.CustomerService;
import com.getir.aliomers.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Api(produces = "application/json", tags = {"05 - Statistics Controller"})
@RequestMapping("/stats")
public class StatisticsController {

    final static Logger LOGGER = LoggerFactory.getLogger(StatisticsController.class);

    @Autowired
    private StatisticsService statisticsService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderMapper orderMapper;


    @GetMapping(path = "/{customerId}/monthly", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaginationResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CommonExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommonExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = CommonExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommonExceptionResponse.class)
    })
    @ApiOperation(value = "List Customer's Monthly Order Statistics With Pagination", notes = "List Customer's Monthly Order Statistics with pagination")
    public ResponseEntity listCustomerMonthlyOrderStats(
            @Valid @PathVariable String customerId) {

        try {
            Optional<Customer> customerOpt = customerService.findByUid(customerId);
            if (customerOpt.isEmpty()) {
                LOGGER.error(ErrorCodesContainer.STATISTICS_ERROR.E05001.getMessage(customerId));
                return new ResponseEntity<>(
                        new CommonExceptionResponse(
                                HttpStatus.NOT_FOUND,
                                ErrorCodesContainer.STATISTICS_ERROR.E05001,
                                customerId),
                        HttpStatus.NOT_FOUND
                );
            }

            List<StatisticsDisplay> customersMonthlyStats = statisticsService.getCustomersMonthlyStats(customerOpt.get().getUid());
            return new ResponseEntity<>(customersMonthlyStats, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(ErrorCodesContainer.STATISTICS_ERROR.E05000.getMessage(), e);
            return new ResponseEntity<>(
                    new CommonExceptionResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            ErrorCodesContainer.STATISTICS_ERROR.E05000,
                            e.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
