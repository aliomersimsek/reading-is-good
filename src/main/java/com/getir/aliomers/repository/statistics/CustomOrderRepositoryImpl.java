package com.getir.aliomers.repository.statistics;

import com.getir.aliomers.entity.Order;
import com.getir.aliomers.model.statistics.StatisticsDisplay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

public class CustomOrderRepositoryImpl implements CustomOrderRepository {

    @Autowired
    private MongoOperations mongoOperations;

    public List<StatisticsDisplay> getStats(String customerId) {
        LookupOperation lookupOperation = LookupOperation.newLookup().
                from("customer").
                localField("uid").
                foreignField("customerId").
                as("cust");

        Aggregation aggregation = newAggregation(
                match(Criteria.where("customerId").is(customerId)),
                lookupOperation,
                group("orderMonth")
                        .count().as("totalOrderCount")
                        .sum("bookCount").as("totalBookCount")
                        .sum("totalAmount").as("totalPurchasedAmount")
                        .addToSet("orderMonth").as("month")
        );

        AggregationResults<StatisticsDisplay> groupResults = mongoOperations.aggregate(
                aggregation, Order.class, StatisticsDisplay.class);

        return groupResults.getMappedResults();
    }
}
