package com.getir.aliomers.repository.statistics;

import com.getir.aliomers.model.statistics.StatisticsDisplay;

import java.util.List;

public interface CustomOrderRepository {

    List<StatisticsDisplay> getStats(String customerId);

}
