package com.getir.aliomers.repository;

import com.getir.aliomers.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepository extends MongoRepository<Book, String> {

    Optional<Book> findByUid(String uid);

    Optional<Book> findByName(String name);

    Optional<Book> findByIsbn(String isbn);

    Page<Book> findAll(Pageable pageable);

    void deleteByUid(String uid);

}
