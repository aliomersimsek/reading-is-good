package com.getir.aliomers.repository;

import com.getir.aliomers.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

    Optional<Customer> findByUid(String uid);

    Optional<Customer> findByEmail(String email);

    Page<Customer> findAll(Pageable pageable);

    void deleteByUid(String uid);

}
