package com.getir.aliomers.repository;

import com.getir.aliomers.entity.Order;
import com.getir.aliomers.repository.statistics.CustomOrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends MongoRepository<Order, String>, CustomOrderRepository {

    Optional<Order> findByUid(String uid);

    Page<Order> findAllByCustomerId(String customerId, Pageable pageable);

    Page<Order> findAllByOrderDateBetween(Date from, Date to, Pageable pageable);

    Page<Order> findAll(Pageable pageable);

    List<Order> findAllByBookId(String bookId);

    List<Order> findAllByCustomerId(String customerId);

}
