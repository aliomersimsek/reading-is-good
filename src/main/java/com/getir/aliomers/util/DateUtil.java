package com.getir.aliomers.util;

import com.getir.aliomers.model.Month;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Month getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        return Month.getByNumber(month);
    }
}
