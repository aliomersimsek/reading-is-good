# reading-is-good
- **Developer**: Ali Ömer Şimşek
- **Date**: 2021-12-12
- **Url**: https://gitlab.com/aliomersimsek/reading-is-good

## General Information
ReadingIsGood is an online books retail firm which operates only on the Internet. Main target of ReadingIsGood is to deliver books from its one centralized warehouse to their customers within the same day. That is why stock consistency is the first priority for their vision operations.

## Languages and Tools

<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/java.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/spring-boot.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/mongodb.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/swagger.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/maven.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/docker.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/postman.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/mapstruct.png"></code>
<code><img height="50" src="https://gitlab.com/aliomersimsek/reading-is-good/-/raw/main/readme-ss/logos/docker-compose.png"></code>

## API
In this project, User uses the api with his/her credentials to get "bearer" token and uses the token for the endpoints.
Sample users:
- user: selimpusat@email.com
  pass: selim.pusat

- user: brucewayne@email.com
  pass: bruce.wayne

### There are 5 controllers in the api.
- Authentication Controller
- Customer Controller
- Book Controller
- Order Controller
- Statistics Controller

- **Swagger UI URL** {{app-url}}/swagger-ui.html

![](readme-ss/swagger-ui-ss.png)

### Endpoints
- login
- manage customer
    - List Customers With Pagination
    - Create Customer
    - Get Customer By Id
    - Delete Customer By Id
    - Update Customer By Id
    - List Customer's Orders
- manage books
    - List Books With Pagination
    - Create Book
    - Get Book By Id
    - Delete Book By Id
    - Update Book By Id
    - Update Book Stock
- manage orders
    - List Orders With Pagination
    - Create Order
    - List Orders By Date Interval With Pagination
    - Get Order By Id
    - Cancel Order
    - Update Order Status
- show statistics
    - List Customer's Monthly Order Statistics With Pagination
    - update order status
    - cancel
    - get/list

![](readme-ss/swagger-ui-endpoints-ss.png)


## Docker
The application is dockerized with Docker Compose.

## How to run

Runs these commands:

<code> > `cd {project-folder}`</code>

<code> > `mvn clean install`</code>

<code> > `docker-compose up -d`</code>

![](readme-ss/docker-container-ss.png)

## Test
The application has unit tests in postman collection.

**Postman file location**: https://gitlab.com/aliomersimsek/reading-is-good/-/blob/main/postman/reading-is-good.postman_collection.json

**Postman runner result location**: https://gitlab.com/aliomersimsek/reading-is-good/-/blob/main/postman/reading-is-good.postman_test_run.json

![](readme-ss/postman-runner-ss.png)

## Error Handling

```
{
    "timestamp": 1639329484889,
    "error": "Not Found",
    "message": "Customer is not found with id: 61b5fc29685cc808e0bae1db",
    "code": "E02002"
}
```

### Error codes overview
- E01 - USER_ERROR
- E02 - CUSTOMER_ERROR
- E03 - BOOK_ERROR
- E04 - ORDER_ERROR
- E05 - STATISTICS_ERROR

### Error codes and messages
| CODE   | MESSAGE                                                                                        |
|--------|------------------------------------------------------------------------------------------------|
| E01000 | Error : {1}.                                                                                   |
| E02000 | Error: {1}                                                     									              |
| E02001 | This email used by another customer: {1}                                                  		  |
| E02002 | Customer is not found with id: {1}                                                 				    |
| E02003 | It is not possible to delete the customer({1}) because there are orders belongs to!            |
| E03000 | Error: {1}  																						                                        |
| E03001 | This book is already added with name: {1}  														                        |
| E03002 | Book is not found with id: {1}  																	                              |
| E03003 | This book is already added with ISBN: {1}  														                        |
| E03004 | It is not possible to delete the book({1}) because there are orders belongs to!  				      |
| E04000 | Error: {1}																					   	                                        |
| E04001 | Book is not found with id: {1}																	                                |
| E04002 | Customer is not found with id: {1}																                              |
| E04003 | Order is not found with id: {1}																	                              |
| E04005 | This book is out of stock that its name is: {1}													                      |
| E04006 | Stock of this book is less than requested. Remaining stock: {1}									              |
| E04007 | The status of the order to be canceled must be 'CREATED'. This order's status is '{1}'			    |
| E04010 | To date must be greater than from date! (From: '{1}' - To: '{2}')								              |
| E04011 | Order status must be valid! (Valid Order Statuses : {1})											                  |
| E04012 | Order current status must be changeable! (Changeable Order Statuses : {1})						          |
| E05000 | Error: {1}																						                                          |
| E05001 | Customer is not found with id: {1}																                              |


