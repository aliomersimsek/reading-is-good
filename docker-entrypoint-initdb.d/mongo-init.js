print('START #################################################################');

db = db.getSiblingDB('readingisgooddb');

print('### USER');
db.createCollection('user');
db.user.insert({
    'email':'selimpusat@email.com',
    'password':'$2a$12$kLl6L1XviinB.3UwbhpAkuaCGseVaSVgTPRxvBGMC3gvhQvEqmefy',
    'firstname':'Selim',
    'lastname':'Pusat'
});
db.user.insert({
    'email':'brucewayne@email.com',
    'password':'$2a$12$Ib4dImIW4SnJZ5oUxiUQgOrq4B7cGJrwYBQl0jpwhWYX0U3vFQ/G6',
    'firstname':'Bruce',
    'lastname':'Wayne'
});

print('### CUSTOMER');
db.createCollection('customer');
db.customer.insert({
    'uid':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'email':'yasinsimsek@email.com',
    'firstname':'Yasin',
    'lastname':'Şimşek',
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.customer.insert({
    'uid':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'email':'sedademirok@email.com',
    'firstname':'Seda',
    'lastname':'Demirok',
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.customer.insert({
    'uid':'09f21df0-9a8e-4e0f-be5b-b449e2f023f4',
    'email':'alisimsek@email.com',
    'firstname':'Ali',
    'lastname':'Şimşek',
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.customer.insert({
    'uid':'b39920e0-4593-4e28-9867-28fb6f2bd8f0',
    'email':'omersimsek@email.com',
    'firstname':'Ömer',
    'lastname':'Şimşek',
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});

print('### BOOK');
db.createCollection('book');
db.book.insert({
    'uid':'311f3103-8359-450d-b4aa-baf12e127867',
    'name':'Ruh Adam',
    'isbn':'9786254081613',
    'author':'Hüseyin Nihal Atsız',
    'publisher':'Ötüken Neşriyat',
    'numberOfStock': new NumberLong(22),
    'price': new NumberDecimal(39.00),
    'creationDate': new Date(2021,8,10,2,35,12),
    'updateDate': new Date(2021,8,10,2,35,12)
});
db.book.insert({
    'uid':'c9a8270a-21c6-4f38-aab1-b0b68a856fa9',
    'name':'Dokuzuncu Hariciye Koğuşu',
    'isbn':'9789754370485',
    'author':'Peyami Safa',
    'publisher':'Ötüken Neşriyat',
    'numberOfStock': new NumberLong(61),
    'price': new NumberDecimal(20.00),
    'creationDate': new Date(2021,8,10,2,35,12),
    'updateDate': new Date(2021,8,10,2,35,12)
});
db.book.insert({
    'uid':'f2ce714a-a3de-4ad9-9ed7-2adf4c15698f',
    'name':'Güçlü Bir Yaşam İçin Öneriler',
    'isbn':'9786057635839',
    'author':'Doğan Cüceloğlu',
    'publisher':'Kronik Kitap',
    'numberOfStock': new NumberLong(12),
    'price': new NumberDecimal(19.89),
    'creationDate': new Date(2021,8,10,2,35,12),
    'updateDate': new Date(2021,8,10,2,35,12)
});
db.book.insert({
    'uid':'fc8fc2e9-3cad-43d3-a3e3-c0329ec6d4bb',
    'name':'Şeker Portakalı',
    'isbn':'9789750738609',
    'author':'Jose Mauro De Vasconcelos',
    'publisher':'Can Yayınları',
    'numberOfStock': new NumberLong(52),
    'price': new NumberDecimal(15.00),
    'creationDate': new Date(2021,8,10,2,35,12),
    'updateDate': new Date(2021,8,10,2,35,12)
});
db.book.insert({
    'uid':'43ac45e4-fd62-498d-b213-adb750485a09',
    'name':'Türkiye Teşkilat ve İdare Tarihi',
    'isbn':'9789757352105',
    'author':'Prof. Dr. İlber Ortaylı',
    'publisher':'Cedit Neşriyat',
    'numberOfStock': new NumberLong(41),
    'price': new NumberDecimal(42.00),
    'creationDate': new Date(2021,8,10,2,35,12),
    'updateDate': new Date(2021,8,10,2,35,12)
});

print('### ORDER');
db.createCollection('order');
db.order.insert({
    'uid':'d3157de1-f757-4c12-b2b7-8622d94fd644',
    'number':'41e3fe92-c292-46b7-98e5-558cec4ffb2d',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'bookCount':new NumberLong(2),
    'totalAmount': new NumberDecimal(11.22),
    'status': 'COMPLETED',
    'orderMonth': 'OCTOBER',
    'orderDate': new Date(2021,9,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'28b23f95-4114-4cc7-8240-6f92b04dd309',
    'number':'d8825df8-8cda-4184-86e9-97b350a94723',
    'bookId':'fc8fc2e9-3cad-43d3-a3e3-c0329ec6d4bb',
    'customerId':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'bookCount':new NumberLong(4),
    'totalAmount': new NumberDecimal(233.00),
    'status': 'COMPLETED',
    'orderMonth': 'OCTOBER',
    'orderDate': new Date(2021,9,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'eb6d3898-b318-4d1a-84b8-1398ff3aa1d6',
    'number':'f920ac2f-64f6-4a16-acd0-18c4c5ca5a92',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'bookCount':new NumberLong(2),
    'totalAmount': new NumberDecimal(12.00),
    'status': 'COMPLETED',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'8102eb75-f673-4eaa-985b-c03987dda2a7',
    'number':'5e3f70ce-8819-4c54-83f9-aeef5fdb67f1',
    'bookId':'f2ce714a-a3de-4ad9-9ed7-2adf4c15698f',
    'customerId':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'bookCount':new NumberLong(1),
    'totalAmount': new NumberDecimal(1235.00),
    'status': 'COMPLETED',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'114e68fd-2835-4a55-b848-cc75257071c8',
    'number':'3a1b805c-a1dd-4014-8606-594bf30f40c9',
    'bookId':'c9a8270a-21c6-4f38-aab1-b0b68a856fa9',
    'customerId':'92820118-af7a-4e0a-b1c3-7a7904210845',
    'bookCount':new NumberLong(2),
    'totalAmount': new NumberDecimal("123.00"),
    'status': 'COMPLETED',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'3d1eb634-2b48-4f4a-869b-1a4744d9870f',
    'number':'5f6a9058-d0d0-4f9c-ae6d-70558eb7fe42',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(4),
    'totalAmount': new NumberDecimal("123.00"),
    'status': 'COMPLETED',
    'orderMonth': 'OCTOBER',
    'orderDate': new Date(2021,9,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'6cf9a7c7-1c7c-49a2-80af-31449da1a9bd',
    'number':'27bebc50-2fa9-42c9-b259-a543c7a9626b',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(6),
    'totalAmount': new NumberDecimal("53.00"),
    'status': 'SHIPPING',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'63d8f8b4-d073-445d-9159-a929d8d2a24d',
    'number':'0bcc651f-486d-4868-b94b-4cd4707bee7d',
    'bookId':'fc8fc2e9-3cad-43d3-a3e3-c0329ec6d4bb',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(1),
    'totalAmount': new NumberDecimal("13.00"),
    'status': 'COMPLETED',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'d5671964-cf13-4fe8-8a3e-9cb3df2ae060',
    'number':'6db26a42-5f2c-4b65-b854-7c52378a2c21',
    'bookId':'f2ce714a-a3de-4ad9-9ed7-2adf4c15698f',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(4),
    'totalAmount': new NumberDecimal("123.22"),
    'status': 'COMPLETED',
    'orderMonth': 'NOVEMBER',
    'orderDate': new Date(2021,10,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'ebfd1d34-7d7b-4993-8814-cdcf02bfc127',
    'number':'1cd4b270-8f17-4047-b251-3ebe77509d17',
    'bookId':'c9a8270a-21c6-4f38-aab1-b0b68a856fa9',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(1),
    'totalAmount': new NumberDecimal("13.22"),
    'status': 'SHIPPING',
    'orderMonth': 'DECEMBER',
    'orderDate': new Date(2021,11,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'2e8a4be3-bdb2-4608-9571-48a905246bfd',
    'number':'f47f7c33-8e93-4a8e-a303-8db21ca7716a',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'8658fe71-bfcc-4f8d-98d7-8af27dc48695',
    'bookCount':new NumberLong(12),
    'totalAmount': new NumberDecimal("132.22"),
    'status': 'SHIPPING',
    'orderMonth': 'DECEMBER',
    'orderDate': new Date(2021,11,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});
db.order.insert({
    'uid':'da14cb4c-3277-4174-9892-715344b1085a',
    'number':'491477b0-cf1a-4741-b373-a461172a0858',
    'bookId':'311f3103-8359-450d-b4aa-baf12e127867',
    'customerId':'09f21df0-9a8e-4e0f-be5b-b449e2f023f4',
    'bookCount':new NumberLong(12),
    'totalAmount': new NumberDecimal("1232.22"),
    'status': 'COMPLETED',
    'orderMonth': 'AUGUST',
    'orderDate': new Date(2021,7,10,2,35,12),
    'creationDate': new Date(2021,11,10,2,35,12),
    'updateDate': new Date(2021,11,10,2,35,12)
});

print('END #################################################################');
